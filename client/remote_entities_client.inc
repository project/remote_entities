<?php

/**
 * Checks if an entity is shared or not.
 *
 * @param string $type
 * @param int $id
 * @param int $revision
 * @return mixed
 *  Returns the origin if the entity is remote,
 *  else returns false.
 */
function entity_is_remote($type, $id, $revision = NULL) {
  $query = db_select('remote_entity_information', 'rei');
  $query->addField('rei', 'origin');
  $query
    ->condition('type', $type)
    ->condition('id', $id);
  if($revision === NULL) {
    $query->isNull('revision');
  }
  else {
    $query->condition('revision', $revision);
  }

  return $query->execute()->fetchField();
}

function remote_entities_get_local_id($remote_id) {
  return db_query('SELECT eu.id
    FROM {entity_uuid} eu
    JOIN {remote_entity_information} rei USING (uuid)
    WHERE rei.id = :id', array(
      ':id' => $remote_id,
    ))->fetchField();
}

/**
 * Saves a remote entity.
 *
 * @param EntityPackage $package
 * @param String $bundle_override
 */
function remote_entity_save($package, $bundle_override = NULL, $detect_dependencies = TRUE) {
  $save = function($type, $entity) {
    $result = entity_save($type, $entity);
    if($result === NULL) { // entity saving failed
      // trying to fall back...
      if(function_exists("{$type}_save")) {
        call_user_func("{$type}_save", $entity);
      }
    }
  };
  $dependencyDetector = function($package, &$entity) {
    $hook = module_invoke_all('remote_entity_managers');
    $client = remote_entities_get_client($package->getOrigin());
    $typeinfo = $client->type();
    foreach($hook as $type => $class) {
      $manager = remote_entities_get_manager($type);
      if($manager) {
        $dependencies = $manager->detectDependency($package->getEntity(), $typeinfo->fields);
        foreach($dependencies as $dep) {
          $pkg = unserialize(base64_decode($client->getEntity($type, $dep)));
          $savedEntity = (array) remote_entity_save($pkg);
          $manager->override($entity,
            $pkg->getID(), $savedEntity[$pkg->getTypeInfo()->getIDKey()],
            $typeinfo->fields);
        }
      }
    }
  };
  $txn = db_transaction();
  try {
    $localentity = clone $package->getEntity();
    if(entity_is_remote($package->getType(), $package->getID())) {
      // setting the local IDs
      $record = db_query('SELECT type, id, revision FROM {entity_uuid} WHERE uuid = :uuid',
        array(
          ':uuid' => $package->getUUID(),
        ))->fetchObject();
      $localentity->{$package->getTypeInfo()->getIDKey()} = $record->id;
      if($package->getTypeInfo()->getRevisionKey()) {
        $localentity->{$package->getTypeInfo()->getRevisionKey()} = $record->revision;
      }
      if($bundle_override !== NULL) {
        $localentity->{$package->getTypeInfo()->getBundleKey()} = $bundle_override;
      }

      if($detect_dependencies) {
        $dependencyDetector($package, $localentity);
      }

      $save($package->getType(), $localentity);

      db_update('remote_entity_information')
        ->fields(array(
          'revision' => $package->getRevision(),
        ))
        ->condition('uuid', $package->getUUID())
        ->execute();
    }
    else {
      // setting the IDs to NULL
      $localentity->{$package->getTypeInfo()->getIDKey()} = NULL;
      if($package->getTypeInfo()->getRevisionKey()) {
        $localentity->{$package->getTypeInfo()->getRevisionKey()} = NULL;
      }
      if($bundle_override !== NULL) {
        $localentity->{$package->getTypeInfo()->getBundleKey()} = $bundle_override;
      }

      $localentity->uuid = $package->getUUID();

      if($detect_dependencies) {
        $dependencyDetector($package, $localentity);
      }

      $save($package->getType(), $localentity);

      db_insert('remote_entity_information')
        ->fields(array(
          'type' => $package->getType(),
          'bundle' => $package->getBundleName(),
          'id' => $package->getID(),
          'revision' => (int) $package->getRevision(),
          'origin' => $package->getOrigin(),
          'uuid' => $package->getUUID(),
          'last_update' => REQUEST_TIME,
          'checksum' => $package->getChecksum(),
        ))->execute();
    }
    return $localentity;
  } catch(Exception $ex) {
    $txn->rollback();
    remote_entities_debug($ex->getMessage());
    throw $ex;
  }
}

interface LoginManager {
  public function __construct($logindata);
  public function login($url);
  public static function getForm();
  public static function formValidate($form, &$form_state);
  public static function formSubmit($form, &$form_state);
  public static function formPackCreditentials($values);
}

interface RemoteEntityClient {
  public function __construct($url, $loginmanager);
  public function getCategories();
  public function listCategory($category, $limit = NULL, $offset = NULL);
  public function getEntity($category, $id);
  public function update($type, $entity);
  public function delete($type, $entity);
  public function type();
  public function serverBaseURL();
}

/**
 * Returns an initiated and connected client instance.
 *
 * @param string $url
 * @return RemoteEntityClient
 */
function remote_entities_get_client($url) {
  $cache = &drupal_static(__FUNCTION__);

  if(!isset($cache[$url])) {
    $protocol = xmlrpc($url, 'entity.protocol');

    if($protocol === FALSE) {
      throw new XMLRPCException(xmlrpc_error_msg(), xmlrpc_errno());
    }

    // @todo make somewhat more intelligent the protocol selection method
    $protocol = array_keys($protocol);
    $protocol = array_shift($protocol);
    $logindata = remote_entities_get_logindata($url, $protocol);

    $cache[$url] = _remote_entities_get_client($url, $protocol, $logindata);
  }

  return $cache[$url];
}

function _remote_entities_get_client($url, $protocol, $logindata) {
  $clients = module_invoke_all('entity_client');
  if(isset($clients[$protocol])) {
    // getting the loginamanger
    $managers = module_invoke_all('entity_client_loginmanager', $protocol);
    $manager = NULL;
    switch(count($managers)) {
      case 0:
        throw new InvalidArgumentException(t('This protocol does not have a login manager!'));
        break;
      case 1:
      default: // @todo
        $manager_class = array_shift($managers);
        $manager = new $manager_class($logindata);
        break;
    }
    return new $clients[$protocol]($url, $manager);
  }
  // TODO figure out something
}

function remote_entities_get_logindata($url, $protocol) {
  return unserialize(db_query('SELECT creditentials
    FROM {remote_entity_endpoint_creditential}
    WHERE origin = :origin AND protocol = :protocol', array(
      ':origin' => $url,
      ':protocol' => $protocol,
    ))->fetchField());
}

abstract class AbstractEntityClient implements RemoteEntityClient {
  protected $url;
  protected $loginmanager;

  public function __construct($url, $loginmanager) {
    $this->url = $url;
    $this->loginmanager = $loginmanager;
  }

  public function getURL() {
    return $this->url;
  }

  public static function getRemoteEntity($type, $id) {
    $types = array_keys(entity_get_info());
    if(!in_array($type, $types)) {
      return NULL;
    }

    $local_id = db_query(
      'SELECT eu.id AS id
        FROM {entity_uuid} eu
        JOIN {remote_entity_information} rei USING(uuid)
        WHERE rei.type = :type AND rei.id = :id', array(
          ':type' => $type,
          ':id' => $id,
        ))->fetchField();

    $entity = entity_load($type, array($local_id));
    if(!count($entity)) {
      return NULL;
    }
    $entity = array_shift($entity);

    $info = entity_get_info($type);
    $uuid = db_query(
      'SELECT uuid FROM {entity_uuid} WHERE type = :type AND id = :id', array(
        ':type' => $type,
        ':id' => $local_id,
    ))->fetchField();
    $data = db_query(
      'SELECT id, revision FROM {remote_entity_information} WHERE uuid = :uuid', array(
        ':uuid' => $uuid,
    ))->fetchObject();
    $entity->{$info['entity keys']['id']} = $data->id;
    if(!empty($info['entity keys']['revision'])) {
      $entity->{$info['entity keys']['revision']} = $data->revision;
    }

    return $entity;
  }
}

class XMLRPCException extends Exception {}

abstract class XMLRPCEntityClient extends AbstractEntityClient {
  protected function execute($function, $args = array()) {
    xmlrpc_clear_error();
    $ret = call_user_func_array('xmlrpc', array_merge(array($this->url, $function), $args));
    if($ret === FALSE) {
      throw new XMLRPCException(xmlrpc_error_msg(), xmlrpc_errno());
    }
    return $ret;
  }
}

class FallbackEntityClient extends XMLRPCEntityClient {
  protected $session;

  protected function send($function) {
    if(empty($this->session)) {
      $this->session = $this->loginmanager->login($this->url);
    }

    $args = func_get_args();
    array_shift($args);

    return $this->execute($function, array_merge(array($this->session), $args));
  }

  public function getCategories() {
    return $this->send('entity.categories');
  }

  public function listCategory($category, $limit = NULL, $offset = NULL) {
    return $this->send('entity.list', $category, (int)$limit, (int)$offset);
  }

  public function getEntity($category, $id) {
    return $this->send('entity.request', $category, (int)$id);
  }

  public function update($type, $id) {
    $entity = self::getRemoteEntity($type, $id);
    return $this->send('entity.update', $type, $entity);
  }

  public function delete($type, $entity) {
    return $this->send('entity.delete', $type, $entity);
  }

  public function serverBaseURL() {
    return $this->send('entity.server.baseURL');
  }

  public function type() {
    return (object)$this->send('entity.type');
  }
}

class FallbackLoginManager implements LoginManager {
  protected $username;
  protected $password;

  public function login($url) {
    $result = xmlrpc($url, 'entity.login', $this->username, $this->password);
    return $result;
  }

  public function __construct($logindata) {
    $this->username = $logindata['username'];
    $this->password = $logindata['password'];
  }

  public static function getForm() {
    $f = array();

    $f['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
    );

    $f['password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
    );

    return $f;
  }

  public static function formValidate($form, &$form_state) {
    ;
  }

  public static function formSubmit($form, &$form_state) {
    ;
  }

  public static function formPackCreditentials($values) {
    return array(
      'username' => $values['fallback_username'],
      'password' => $values['fallback_password'],
    );
  }
}

class ServicesEntityClient extends AbstractEntityClient {
  public function getCategories() {
    return NULL;
  }
  public function listCategory($category, $limit = NULL, $offset = NULL) {
    ;
  }
  public function getEntity($category, $id) {
    ;
  }
  public function update($type, $entity) {
    ;
  }
  public function delete($type, $entity) {
    ;
  }
  public function serverBaseURL() {
    ;
  }
  public function type() {
    ;
  }
}