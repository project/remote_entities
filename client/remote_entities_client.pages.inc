<?php

function remote_entities_admin_category_sync_callback($origin, $category, $id) {
  $origin = urldecode($origin);
  module_load_include('api.inc', 'remote_entities');
  module_load_include('inc', 'remote_entities_client');

  return drupal_get_form('remote_entities_admin_category_sync_form',
    $origin, $category, $id);
}

function remote_entities_admin_category_sync_form($form, $form_state, $origin, $category, $id) {
  $client = remote_entities_get_client($origin);
  $package = unserialize(base64_decode($client->getEntity($category, $id)));
  $field_info = field_info_bundles();

  $f = array();

  $f['origin'] = array('#type' => 'value', '#value' => $origin);
  $f['category'] = array('#type' => 'value', '#value' => $category);
  $f['id'] = array('#type' => 'value', '#value' => $id,);
  $f['package'] = array('#type' => 'value', '#value' => $package);

  $f['localtypes'] = array(
    '#type' => 'radios',
    '#title' => t('Clone into'),
    '#default_value' =>
      db_query('SELECT override FROM {remote_entity_bundle} WHERE original = :original', array(
        ':original' => $package->getBundleName(),
      ))->fetchField(),
    '#options' => drupal_map_assoc(array_keys($field_info[$package->getType()])) + array(
      '-' => t('Clone type'),
    ),
  );

  $f['newtype'] = array(
    '#type' => 'textfield',
    '#title' => t('New type name'),
    '#states' => array(
      'visible' => array(
        'input[name=localtypes]' => array('value' => '-'),
      ),
    ),
  );

  $f['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Synchronize'),
  );

  return $f;
}

function remote_entities_admin_category_sync_form_validate($form, &$form_state) {
  $client = remote_entities_get_client($form_state['values']['origin']);
  $field_info = field_info_bundles();
  if($form_state['values']['localtypes'] == '-') {
    $typeinfo = $client->type();
    $bundle = $form_state['values']['package']->getBundleName();
    foreach($typeinfo->fields as $fieldinfo) {
      if(in_array($bundle, $fieldinfo['bundles']) && !module_exists($fieldinfo['module'])) {
        form_set_error('localtypes',
          t('Unable to clone content type, module %module is missing.'), array(
            '%module' => $fieldinfo['module'],
          ));
      }
    }
    $machine_name = preg_replace('#[^a-z0-9]+#i', '_', $form_state['values']['newtype']);
    if(in_array($machine_name, array_keys($field_info[$form_state['values']['category']]))) {
      form_set_error('newtype',
        t('Bundle %bundle is already exists.', array(
          '%bundle' => $machine_name,
        )));
    }
  }
}

function remote_entities_admin_category_sync_form_submit($form, &$form_state) {
  $txn = db_transaction();
  try {
    if($form_state['values']['localtypes'] == '-') {
      $type = preg_replace('#[^a-z0-9]+#i', '_', $form_state['values']['newtype']);
      $bundle = new stdClass();
      $bundle->type = $type;
      $bundle->name = $form_state['values']['newtype'];
      remote_entities_get_manager($form_state['values']['category'])
        ->createBundle($bundle);
      $original_bundle = $form_state['values']['package']->getBundleName();
      db_insert('remote_entity_bundle')
        ->fields(array(
          'original' => $original_bundle,
          'override' => $type,
        ))->execute();
      // copying fields
      $client = remote_entities_get_client($form_state['values']['origin']);
      $typeinfo = $client->type();
      $fields = array();
      foreach($typeinfo->fields as $name => $field) {
        if(isset($field['bundles'][$form_state['values']['category']]) &&
            in_array($original_bundle, $field['bundles'][$form_state['values']['category']])) {
          $fields[$name] = $field;
        }
      }
      $localfields = field_info_fields();
      foreach($fields as $field_name => $field_data) {
        if(!isset($localfields[$field_name])) {
          foreach(array(
              'storage', 'bundles', 'columns', 'deleted', 'module', 'indexes'
              ) as $disabled_key) {
            unset($field_data[$disabled_key]);
          }
          field_create_field($field_data);
        }
        else {
          if(in_array($type, $localfields[$field_name]['bundles'][$form_state['values']['category']])) {
            continue;
          }
        }
        $instancedata = $typeinfo->instances
          [$form_state['values']['category']][$original_bundle][$field_name];
        $instancedata['bundle'] = $type;
        field_create_instance($instancedata);
      }
    }
    _remote_entities_sync_callback(
      $form_state['values']['origin'],
      $form_state['values']['package'],
      $form_state['values']['localtypes'] == '-' ?
        $type : $form_steate['values']['localtypes']);
    throw new Exception;
  } catch (Exception $ex) {
    $txn->rollback();
  }
}

function _remote_entities_sync_callback($origin, $package, $bundle_override = NULL) {
  module_load_include('api.inc', 'remote_entities');
  module_load_include('inc', 'remote_entities_client');

  remote_entity_save($package, $bundle_override, TRUE);

  drupal_goto('admin/config/services/remote-entities-client/' .
    urlencode($origin) . '/category/' . check_url($package->getType()));
}

function remote_entities_admin_category_unsync_callback($origin, $category, $id) {
  $origin = urldecode($origin);
  $types = array_keys(entity_get_info());
  module_load_include('api.inc', 'remote_entities');
  module_load_include('inc', 'remote_entities_client');

  if(in_array($category, $types)) {

    $local_id = db_query('SELECT eu.id
      FROM {entity_uuid} eu
      JOIN {remote_entity_information} rei USING (uuid)
      WHERE rei.type = :type AND rei.id = :id', array(
        ':type' => $category,
        ':id' => $id,
      ))->fetchField();

    $result = entity_delete($category, $local_id);
    if($result === FALSE) {
      if(function_exists("{$category}_delete")) {
        call_user_func("{$category}_delete", $local_id);
      }
      else {
        drupal_set_message(t('Unable to delete the local entity.'), 'error');
      }
    }

  }
  else {
    drupal_set_message(t('Invalid category.'), 'error');
  }

  drupal_goto('admin/config/services/remote-entities-client/' .
    urlencode($origin) . '/category/' . check_url($category));
}

function remote_entities_admin_category_update_callback($origin, $category, $id) {
  $origin = urldecode($origin);
  module_load_include('api.inc', 'remote_entities');
  module_load_include('inc', 'remote_entities_client');

  $client = remote_entities_get_client($origin);

  $client->update($category, $id);

  drupal_goto('admin/config/services/remote-entities-client/' .
    urlencode($origin) . '/category/' . check_url($category));
}

function remote_entities_admin_category_delete_callback($origin, $category, $id) {
  $origin = urldecode($origin);
  module_load_include('api.inc', 'remote_entities');
  module_load_include('inc', 'remote_entities_client');

  $client = remote_entities_get_client($origin);

  $client->delete($category, $id);

  drupal_goto('admin/config/services/remote-entities-client/' .
    urlencode($origin) . '/category/' . check_url($category));
}

function remote_entities_admin_client_page() {
  $output = '';

  $rows = array();
  $res = db_query('SELECT * FROM {remote_entity_endpoint_creditential} ORDER BY origin');
  foreach($res as $row) {
    $rows []= array(
      $row->origin,
      $row->protocol,
      l(t('View contents'), 'admin/config/services/remote-entities-client/' .
        urlencode($row->origin) . '/view'),
      l(t('Delete'), 'admin/config/services/remote-entities-client/' .
        urlencode($row->origin) . '/delete'),
    );
  }

  $output .= theme('table', array(
    'header' => array(t('Origin'), t('Protocol'), '&nbsp;', '&nbsp;'),
    'rows' => $rows,
  ));

  $output .= drupal_render(drupal_get_form('remote_entities_client_settings_form'));

  return $output;
}

function remote_entities_admin_view_callback($origin) {
  $origin = urldecode($origin);
  $items = array();

  module_load_include('inc', 'remote_entities_client');
  $client = remote_entities_get_client($origin);

  drupal_set_title(t('Available categories on @host', array('@host' => $origin)));

  foreach($client->getCategories() as $name => $data) {
    $items[] = l($name,
      'admin/config/services/remote-entities-client/' .
      urlencode($origin) . '/category/' . urlencode($name));
  }

  return theme('item_list', array(
    'items' => $items,
  ));
}

function remote_entities_admin_category_callback($origin, $category) {
  $origin = urldecode($origin);

  module_load_include('api.inc', 'remote_entities');
  module_load_include('inc', 'remote_entities_client');
  $client = remote_entities_get_client($origin);
  $categories = $client->getCategories(); // @todo cache

  $entities = $client->listCategory($category);
  $entities = array_map(function($item) {
    return unserialize(base64_decode($item));
  }, $entities);
  drupal_set_title(t('Available contents in the @category category.', array(
    '@category' => $category,
  )));

  $base_url = $client->serverBaseURL();

  $rows = array();

  foreach($entities as $package) {
    if(!$package instanceof EntityPackage) {
      watchdog('remote entities error', t('Undefined remote data'), $package, WATCHDOG_ERROR);
      continue;
    }
    $entity = $package->getEntity();
    $local_id = remote_entities_get_local_id(
      $entity->{$categories[$category]['entity keys']['id']});
    $rows[] = array(
      $entity->{$categories[$category]['entity keys']['id']},
      $entity->{$categories[$category]['name property']},
      $local_id === FALSE ?
        l(t('Synchronize'),
          'admin/config/services/remote-entities-client/' .
            urlencode($origin) . '/category/' . $category . '/sync/' .
            $entity->{$categories[$category]['entity keys']['id']}):
        l(t('Unsynchronize'),
          'admin/config/services/remote-entities-client/' .
            urlencode($origin) . '/category/' . $category . '/unsync/' .
            $entity->{$categories[$category]['entity keys']['id']}),
      $local_id === FALSE ?
        t('Not sychronized'):
        l(t('Force update'),
          'admin/config/services/remote-entities-client/' .
            urlencode($origin) . '/category/' . $category . '/update/' .
            $entity->{$categories[$category]['entity keys']['id']}),
      $local_id === FALSE ?
        t('Not synchronized'):
        l(t('Delete remote entity'),
          'admin/config/services/remote-entities-client/' .
            urlencode($origin) . '/category/' . $category . '/delete/' .
            $entity->{$categories[$category]['entity keys']['id']}),
      l(t('Jump to the original edit form'),
        "{$base_url}/{$category}/" . $entity->{$categories[$category]['entity keys']['id']} . '/edit'),
    );
  }

  return theme('table', array(
    'header' => array(
      t('Id'), t('Name'),
      t('Status'), t('Update'), t('Delete'), t('Edit'),
    ),
    'rows' => $rows,
  ));
}

function remote_entities_admin_delete_callback($origin) {
  // @todo put a form here to avoid CSRF attacks
  $origin = urldecode($origin);
  db_delete('remote_entity_endpoint_creditential')
    ->condition('origin', $origin)
    ->execute();
  drupal_goto('admin/config/services/remote-entities-client');
}

/**
 * Generates radio buttons.
 *
 * @param string $title
 * @param string $hook
 * @return array
 */
function _remote_entities_create_radios($title, $hook, $variable_name) {
  $items = module_invoke_all($hook);
  return array(
    '#type' => 'radios',
    '#title' => $title,
    '#default_value' => variable_get($variable_name),
//    '#description' => theme('item_list', array(
//      'items' => array_map(function($item) {
//        return t('%option: @description',
//          array('%option' => $item['title'], '@description' => $item['description']));
//      }, $items),
//    )),
    '#options' => array_map(function($item) {
      return $item['title'];
    }, $items),
  );
}

/**
 * Page callback for admin/config/services/remote-entities
 *
 * @return array
 */
function remote_entities_client_settings_form() {
  $form = array();

  $form['remote_entities_update_strategy'] =
    _remote_entities_create_radios(
      t('Update strategy'), 'update_strategy', 'remote_entities_update_strategy'
    );

  $form['remote_entities_update_method'] =
    _remote_entities_create_radios(
      t('Update method'), 'update_method', 'remote_entities_update_method'
    );

  $form['remote_entities_editing_method'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Editing method'),
    '#default_value' => variable_get('remote_entities_editing_method'),
    '#options' => array(
      'direct' => t('Automatic update'),
      'redirect' => t('Add link to the original content\'s edit form'),
    ),
  );

  return system_settings_form($form);
}

function remote_entities_client_add_form($form, &$form_state) {
  if(isset($form_state['rebuild']) && $form_state['rebuild']) {
    $form_state['input'] = array();
  }
  if(empty($form_state['storage'])) {
    $form_state['storage'] = array(
      'step' => 'remote_entities_client_add_form_start',
    );
  }

  $function = $form_state['storage']['step'];
  return $function($form, $form_state);
}

function remote_entities_client_add_form_validate($form, &$form_state) {
  if (function_exists($form_state['storage']['step'] . '_validate')) {
    $function = $form_state['storage']['step'] . '_validate';
    $function($form, $form_state);
  }
}

function remote_entities_client_add_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if(isset($values['back']) && $values['op'] == 'back') {
    $step = $form_state['storage']['step'];
    if(function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
    $last_step = array_pop($form_state['storage']['steps']);
    $form_state['storage']['step'] = $last_step;
  }
  else {
    $step = $form_state['storage']['step'];
    $form_state['storage']['steps'] []= $step;
    if(function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
  }
}

function remote_entities_client_add_form_start($form, &$form_state) {
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint URL'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
  );

  return $form;
}

function remote_entities_client_add_form_start_validate($form, &$form_state) {
  $xmlrpc_result = xmlrpc($form_state['values']['url'], 'entity.protocol');
  if($xmlrpc_result === FALSE) {
    form_set_error('url', t('Invalid endpoint. %errno: @message', array(
      '%errno' => xmlrpc_errno(),
      '@message' => xmlrpc_error()->message,
    )));
  }
  else {
    $form_state['storage']['xmlrpc_result'] = $xmlrpc_result;
  }
}

function remote_entities_client_add_form_start_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $values = $form_state['values'];
  if (isset($values['back']) && $values['op'] == $values['back']) {
    $form_state['storage']['url'] = NULL;
  }
  else {
    $form_state['storage']['url'] = $values['url'];
    $form_state['storage']['step'] = 'remote_entities_client_add_form_settings';
  }
}

function remote_entities_client_add_form_settings($form, &$form_state) {
  $servers = $form_state['storage']['xmlrpc_result'];

  $form['protocol'] = array(
    '#type' => 'radios',
    '#title' => t('Connection method'),
    '#options' => drupal_map_assoc(array_keys($servers)),
  );

  $form['protocol_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Connection settings'),
    '#collapsible' => FALSE,
  );

  foreach($servers as $protocol => $loginmanagers) {
    $loginmanagers = module_invoke_all('entity_client_loginmanager', $protocol);

    if(isset($loginmanagers[$protocol])) {
      foreach(call_user_func(array($loginmanagers[$protocol], 'getForm')) as $key => $item) {
        $form['protocol_settings']["{$protocol}_{$key}"] = $item + array(
          '#states' => array(
            'visible' => array(
              ':input[name=protocol]' => array('value' => $protocol),
            ),
          )
        );
      }
    }
  }

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#weight' => 10,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function remote_entities_client_add_form_settings_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!(isset($values['back']) && $values['op'] == $values['back'])) {
    $protocol = $values['protocol'];
    $loginmanagers = module_invoke_all('entity_client_loginmanager', $protocol);
    if(isset($loginmanagers[$protocol])) {
      call_user_func(array($loginmanagers[$protocol], 'formValidate'), $form, $form_state);
    }
  }
}

function remote_entities_client_add_form_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['back']) && $values['op'] == $values['back']) {
    // Only rebuild if moving back.
    $form_state['rebuild'] = TRUE;
  }
  else {
    $form_state['rebuild'] = FALSE;
    $form_state['redirect'] = 'admin/config/services/remote-entities';
    $protocol = $values['protocol'];
    $loginmanagers = module_invoke_all('entity_client_loginmanager', $protocol);
    if(isset($loginmanagers[$protocol])) {
      call_user_func(array($loginmanagers[$protocol], 'formSubmit'), $form, $form_state);
    }
    db_insert('remote_entity_endpoint_creditential')
      ->fields(array('origin', 'protocol', 'creditentials'))
      ->values(array(
        'origin' => $form_state['storage']['url'],
        'protocol' => $protocol,
        'creditentials' =>
          serialize(call_user_func(array($loginmanagers[$protocol], 'formPackCreditentials'), $values)),
      ))
      ->execute();
  }
}
