<?php

function remote_entities_debug($message, array $variables = array()) {
  return watchdog('remote entities debug', $message, $variables, WATCHDOG_DEBUG);
}

/**
 * Checks if the services module is available.
 *
 * @todo check if the Drupal 7 port of the services
 * is ready for development
 *
 * @return bool
 */
function remote_entities_services_available() {
  return FALSE;
  //return module_exists('services');
}

/**
 * Returns the assigned UUID for an entity.
 *
 * If the UUID does not exist then it will be
 * automatically created.
 *
 * @param string $type
 * @param int $id
 * @param int $revision
 * @return string
 */
function entity_get_uuid($type, $id, $revision = NULL) {
  $query = db_select('entity_uuid', 'eu');
  $query->addField('eu', 'uuid');
  $query
    ->condition('type', $type)
    ->condition('id', $id);
  if($revision === NULL) {
    $query->isNull('revision');
  }
  else {
    $query->condition('revision', $revision);
  }

  $result = $query->execute();

  $uuid = $result->fetchField();

  if($uuid === FALSE) {
    module_load_include('uuid.inc', 'remote_entities');
    $uuid = remote_entities_get_uuid();

    db_insert('entity_uuid')
      ->fields(array('type', 'id', 'revision', 'uuid'))
      ->values(array($type, $id, $revision, $uuid))
      ->execute();
  }

  return $uuid;
}

interface RemoteEntityManager {
  public function createBundle($bundle);

  /**
   * This function is mostly a placeholder,
   * to make sure that the constructor
   * can be called without arguments.
   */
  public function __construct();

  public function getFields($allfields);

  public function detectDependency($entity, $allfields);

  public function override(&$entity, $id, $newid, $allfields);
}

class RemoteEntityManangerDoesNotExistsException extends Exception {}

/**
 * Returns the entity manager for the given entity type.
 *
 * @param string $entity_type
 * @return RemoteEntityManager
 */
function remote_entities_get_manager($entity_type) {
  $managers = &drupal_static(__FUNCTION__);
  if(!is_object($managers)) {
    $managers = new stdClass();
    $managers->classes = module_invoke_all('remote_entity_managers');
    $managers->instances = array();
  }

  if(!isset($managers->classes[$entity_type])) {
    throw new RemoteEntityManangerDoesNotExistsException(
      t('Manager for @entity-type does not exists.',
        array(
          '@entity-type' => $entity_type,
        )));
  }

  if(!isset($managers->instances[$entity_type])) {
    $managers->instances[$entity_type] = new $managers->classes[$entity_type];
  }

  return $managers->instances[$entity_type];
}

abstract class SerializableInfoObject implements Serializable {
  public function serialize() {
    return serialize(get_object_vars($this));
  }

  public function unserialize($data) {
    foreach(unserialize($data) as $key => $value) {
      $this->{$key} = $value;
    }
  }
}

class EntityBundleFieldInfo extends SerializableInfoObject {
  protected $fields;
  protected $entity_type;
  protected $bundle;

  public function compare(EntityBundleFieldInfo $fieldinfo) {
    // keys to compare: settings, type, active, locked, cardinality
    $reccompare = function($item0, $item1, $reccompare) {
      if(is_array($item0) || is_object($item0)) {
        if(is_array($item1) || is_object($item1)) {
          if(count(array_diff(array_keys($item0), array_keys($item1)))) {
            return FALSE;
          }
          else {
            foreach(array_keys($item0) as $key) {
              if(!$reccompare($item0[$key], $item1[$key], $reccompare)) {
                return FALSE;
              }
            }
          }
        }
        else {
          return FALSE;
        }
      }
      else {
        return $item0 == $item1;
      }
    };

    $cmpfields = $fieldinfo->getFields();

    $diff = array_diff(
      array_keys($this->fields), array_keys($cmpfields));

    foreach(array_keys($this->fields) as $field) {
      foreach(array('settings', 'type', 'active', 'locked', 'cardinality') as $key) {
        if(!$reccompare($this->fields[$key], $cmpfields[$key], $reccompare)) {
          return FALSE;
        }
      }
    }

    return TRUE;
  }

  public function getFields() {
    return $fields;
  }

  public function __construct($entity_type, $bundle, $info_callback = 'field_info_fields') {
    $this->fields = array_filter(is_callable($info_callback) ?
        call_user_func($info_callback) :
        $info_callback,
      function($item) {
        return isset($item['bundles'][$entity_type])
          && in_array($bundle, $item['bundles'][$entity_type]);
      });
    $this->entity_type = $entity_type;
    $this->bundle = $bundle;
  }
}

class EntityTypeInfo extends SerializableInfoObject {
  protected $type;
  protected $type_object;
  protected $fields;

  public function __construct($type) {
    $this->type = $type;
  }

  protected function typeInfo($key) {
    $type = $this->getTypeObject();
    return isset($type[$key]) ? $type[$key] : NULL;
  }

  public function getIDKey() {
    return $this->entityKeys('id');
  }

  public function getRevisionKey() {
    return $this->entityKeys('revision');
  }

  public function getBundleKey() {
    return $this->entityKeys('bundle');
  }

  protected function entityKeys($key) {
    $type = $this->getTypeObject();
    return isset($type['entity keys'][$key]) ? $type['entity keys'][$key] : NULL;
  }

  public function hasRevisions() {
    return $this->getRevisionKey() !== NULL;
  }

  public function hasBundles() {
    return (bool) count($this->getBundles());
  }

  public function getBundles() {
    $type = $this->getTypeObject();
    return isset($type['bundles'])?
      array_keys($type['bundles']):
      array();
  }

  public function getBundle($bundle) {
    $type = $this->getTypeObject();
    return isset($type['bundles'][$bundle])?
      $type['bundles'][$bundle]:
      NULL;
  }

  public function getFields($bundle) {
    if(empty($this->fields)) {
      $this->fields = new EntityBundleFieldInfo($this-type, $bundle);
    }

    return $this->fields;
  }

  public function getTypeObject() {
    if(!isset($this->type_object)) {
      // TODO add error handling
      $this->type_object = entity_get_info($this->type);
    }
    return $this->type_object;
  }

  public function isFieldable() {
    return $this->typeInfo('fieldable');
  }
}

class EntityInfoObject extends SerializableInfoObject {
  protected $entity;
  protected $type;
  protected $typeinfo;

  public function __construct($type, $entity_id) {
    $this->type = $type;
    $entities = entity_load($type, array($entity_id));
    $this->entity = array_shift($entities);
  }

  public function getID() {
    $key = $this->getTypeInfo()->getIDKey();
    return isset($this->entity->$key)?
      $this->entity->$key:
      NULL;
  }

  public function getBundle() {
    $bundle = $this->getBundleName();
    return $bundle ? $this->getTypeInfo()->getBundle($bundle) : NULL;
  }

  public function getBundleName() {
    $key = $this->getTypeInfo()->getBundleKey();
    return isset($this->entity->$key)?
      $this->entity->$key:
      NULL;
  }

  public function getRevision() {
    $key = $this->getTypeInfo()->getRevisionKey();
    return isset($this->entity->$key)?
      $this->entity->$key:
      NULL;
  }

  /**
   * Banana banana banana
   *
   * @return EntityTypeInfo
   */
  public function getTypeInfo() {
    if(!isset($this->typeinfo)) {
      $this->typeinfo = new EntityTypeInfo($this->type);
    }
    return $this->typeinfo;
  }

  public function getType() {
    return $this->type;
  }

  public function getEntity() {
    return $this->entity;
  }
}

class EntityPackage extends EntityInfoObject {
  protected $origin;
  protected $uuid;
  protected $checksum;

  public static function createFromEntity($entity_type, $entity_id) {
    $info = new EntityInfoObject($entity_type, $entity_id);

    module_load_include('inc', 'remote_entities', 'remote_entities.uuid');
    global $base_url;
    $pkg = new EntityPackage(
      entity_get_uuid($entity_type, $entity_id),
      $info->getEntity(),
      $entity_type,
      sha1(serialize($info->getEntity())),
      $base_url . '/xmlrpc.php'
    );
    return $pkg;

  }

  public function __construct($uuid, $entity, $type, $checksum, $origin) {
    $this->uuid = $uuid;
    $this->entity = $entity;
    $this->type = $type;
    $this->checksum = $checksum;
    $this->origin = $origin;
  }

  public function getOrigin() {
    return $this->origin;
  }

  public function getUUID() {
    return $this->uuid;
  }

  public function getChecksum() {
    return $this->checksum;
  }

  public function toStdClass() {
    $ret = new stdClass();

    foreach(get_object_vars($this) as $key => $value) {
      $ret->$key = $value;
    }

    return $ret;
  }
}

class EntityIsNotSharedException extends Exception {}

class NodeRemoteEntityManager implements RemoteEntityManager {

  public function __construct() {}

  public function createBundle($bundle) {
    $type = node_type_set_defaults($bundle);
    $type->base = 'node_content';
    $status = node_type_save($type);
    node_types_rebuild();
    menu_rebuild();
    switch($status) {
      case SAVED_NEW:
        node_add_body_field($type);
        break;
    }
  }

  public function getFields($allfields) {
    return array();
  }

  public function detectDependency($entity, $allfields) {
    return array();
  }

  public function override(&$entity, $id, $newid, $allfields) {
    ;
  }
}

class TaxonomyTermRemoteEntityManager implements RemoteEntityManager {

  public function __construct() {}

  public function createBundle($bundle) {}

  public function getFields($allfields) {
    $fields = array();

    foreach($allfields as $field_name => $field_data) {
      if(isset($field_data['module']) && $field_data['module'] == 'taxonomy') {
        $fields[] = $field_name;
      }
    }

    return $fields;
  }

  public function detectDependency($entity, $allfields) {
    $tids = array();
    $fields = $this->getFields($allfields);

    if(is_object($entity)) {
      $entity = (array) $entity;
    }

    foreach($fields as $field) {
      if(isset($entity[$field])) {
        foreach($entity[$field][$entity['language']] as $tid) {
          $tids[] = $tid['tid'];
        }
      }
    }

    return $tids;
  }

  public function override(&$entity, $id, $newid, $allfields) {
    $fields = $this->getFields($allfields);

    foreach($fields as $field) {
      if(is_array($entity)) {
        if(isset($entity[$field])) {
          foreach($entity[$field][$entity['language']] as $key => $term) {
            if($term['tid'] == $id) {
              $entity[$field][$entity['language']][$key]['tid'] = $newid;
            }
          }
        }
      }
      else {
        if(isset($entity->$field)) {
          foreach($entity->{$field}[$entity->language] as $key => $term) {
            if($term['tid'] == $id) {
              $entity->{$field}[$entity->language][$key]['tid'] = $newid;
            }
          }
        }
      }
    }
  }
}

class TaxonomyVocabularyRemoteEntityManager implements RemoteEntityManager {

  public function __construct() {

  }

  public function createBundle($bundle) {
    return NULL;
  }

  public function detectDependency($entity, $allfields) {
    if(isset($entity['tid']) && isset($entity['vid'])) {
      return array($entity['vid']);
    }
    return array();
  }

  public function getFields($allfields) {
    return array();
  }

  public function override(&$entity, $id, $newid, $allfields) {
    $entity['vid'] = $newid;
  }
}
