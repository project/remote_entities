<?php

/**
 * Interface for UUID generation.
 */
interface RemoteEntitiesUUID {
  /**
   * Returns a UUID.
   */
  public function get();
}

/**
 * UUID generator class.
 */
class NativeUUID implements RemoteEntitiesUUID {

  /**
   * Local instance of an uuid resource.
   *
   * @var resource
   */
  protected $uuid = NULL;

  /**
   * Constructor.
   */
  public function __construct() {
    uuid_create(&$this->uuid);
  }

  /**
   * Inner function to generate an UUID.
   *
   * @param int $constant
   * @return string
   */
  protected function make($constant) {
    $uuidstring = '';
    uuid_make($this->uuid, $constant);
    uuid_export($this->uuid, UUID_FMT_STR, &$uuidstring);
    return trim($uuidstring);
  }

  /**
   * Returns a v1 type UUID.
   *
   * @return string
   */
  public function v1() {
    return $this->make(UUID_MAKE_V1);
  }

  /**
   * Returns a v4 type UUID.
   *
   * @return string
   */
  public function v4() {
    return $this->make(UUID_MAKE_V4);
  }

  /**
   * Alias for uuid::v4().
   *
   * @return string
   */
  public function get() {
    return $this->v4();
  }
}

/**
 * If the UUID extension is not available,
 * then try to emulate it.
 */
class EmulatedUUID implements RemoteEntitiesUUID {

  /**
   * Returns a UUID.
   *
   * @return string
   */
  public function get() {
    $md5 = uniqid(md5(mt_rand()), TRUE);
    return substr($md5, 0, 8) . '-' .
            substr($md5, 8, 4) . '-' .
            substr($md5, 12, 4) . '-' .
            substr($md5, 16, 4) . '-' .
            substr($md5, 20, 12);
  }

}

/**
 * Returns an UUID object.
 *
 * If the php-uuid extension is installed, it
 * returns an object which uses that, else it
 * returns an emulating object.
 *
 * @return RemoteEntitiesUUID
 */
function remote_entities_get_uuid_object() {
  return (function_exists('uuid_create') &&
          function_exists('uuid_make') &&
          function_exists('uuid_export')) ?
          new NativeUUID() :
          new EmulatedUUID();
}

/**
 * Returns a new UUID.
 *
 * @return string
 */
function remote_entities_get_uuid() {
  return remote_entities_get_uuid_object()->get();
}
