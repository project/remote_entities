<?php

function remote_entities_xmlrpc_entity_protocol() {
  module_load_include('api.inc', 'remote_entities');
  return (remote_entities_services_available() ? array(
      'services-xmlrpc' => array(),
    ) : array()) +
    array(
      'fallback' => array('fallback'),
    );
}

function remote_entities_xmlrpc_entity_login($name, $pass) {
  global $user;
  if(!$user->uid) {
    $form_state = array(
      'values' => array(
        'name' => $name,
        'pass' => $pass,
      ),
    );
    drupal_form_submit('user_login', $form_state);
  }
  return session_id();
}

function remote_entities_xmlrpc_update($sid, $entity_type, $entity) {
  remote_entities_xmlrpc_load_session($sid);

  $result = entity_save($entity_type, $entity);
  if($result === NULL) {
    if(function_exists("{$entity_type}_save")) {
      call_user_func("{$entity_type}_save", $entity);
    }
    else {
      return FALSE;
    }
  }
  return TRUE;
}

function remote_entities_xmlrpc_entity_delete($sid, $entity_type, $entity) {
  remote_entities_xmlrpc_load_session($sid);

  $result = entity_delete($entity_type, $entity);
  if($result === NULL) {
    $info = entity_get_info($entity_type);
    if(function_exists("{$entity_type}_delete")) {
      call_user_func("{$entity_type}_delete", $entity->{$info['entity keys']['id']});
    }
    else {
      return FALSE;
    }
  }
  return TRUE;
}

function remote_entities_xmlrpc_entity_server_baseurl($sid) {
  remote_entities_xmlrpc_load_session($sid);
  global $base_url;
  return $base_url; // @todo handle protocol
}

function remote_entities_xmlrpc_entity_lastupdate($sid, $type, $id) {
  remote_entities_xmlrpc_load_session($sid);
  return db_query(
    'SELECT last_update FROM {entity_uuid} WHERE type = :type AND id = :id', array(
      ':type' => $type,
      ':id' => $id,
    ))
    ->fetchField();
}

function remote_entities_xmlrpc_entity_request($sid, $type, $id) {
  remote_entities_xmlrpc_load_session($sid);
  module_load_include('api.inc', 'remote_entities');
  module_load_include('inc', 'remote_entities_server');

  $info = entity_get_info($type);
  // @todo some security discussion about this part
  if(isset($type['access callback'])) {
    $entities = entity_load($type, array($id));
    if(function_exists($type['access callback']) &&
      $type['access callback']('view', (array) array_shift($entities)) === FALSE) {
      throw new PermissionDeniedException();
    }
  }

  if(entity_is_shared($type, $id)) {
    return base64_encode(serialize(EntityPackage::createFromEntity($type, $id)));
  }
  else {
    throw new EntityIsNotSharedException();
  }
}

function remote_entities_xmlrpc_entity_categories($sid) {
  remote_entities_xmlrpc_load_session($sid);
  module_load_include('inc', 'remote_entities_server');

  $ret = array();

  foreach(entity_get_info() as $type => $data) {
    if(!empty($data['entity keys']['bundle'])) {
      foreach($data['bundles'] as $bundle_name => $bundle) {
        // @todo discuss if sharing the whole info structure is necessary
        if(entity_type_get_sharing_policy($type, $bundle_name) != 'share_none') {
          $ret[$type] = $data;
          break;
        }
      }
    }
    else {
      if(entity_type_get_sharing_policy($type) != 'share_none') {
        $ret[$type] = $data;
      }
    }
  }

  return $ret;
}

function remote_entities_xmlrpc_entity_list($sid, $category, $offset = NULL, $limit = NULL) {
  remote_entities_xmlrpc_load_session($sid);
  module_load_include('api.inc', 'remote_entities');
  module_load_include('inc', 'remote_entities_server');

  $info = entity_get_info($category);

  if(!is_array($info)) {
    throw new InvalidArgumentException();
  }

  $query = db_select($info['base table'], 't');
  $query->fields('t', array($info['entity keys']['id']));
  // @todo the pager here is buggy
  if($offset > 0 || $limit > 0) {
    $query->range((int)$offset, (int)$limit);
  }
  $query->orderBy($info['entity keys']['id']);
  $res = $query->execute();

  $ids = $res->fetchCol();

  $entities = array();

  foreach($ids as $i) {
    if(entity_is_shared($category, $i)) {
      $entities[] = base64_encode(serialize(EntityPackage::createFromEntity($category, $i)));
    }
  }

  return $entities;
}

function remote_entities_xmlrpc_entity_type($sid) {
  remote_entities_xmlrpc_load_session($sid);
  $ret = new stdClass();

  $ret->types = entity_get_info();
  $ret->fields = field_info_fields();
  $ret->instances = field_info_instances();

  return $ret;
}

function remote_entities_xmlrpc_load_session($sid) {
  global $user;

  if(!empty($user->sid) && $user->sid == $sid) {
    return $user;
  }

  $backup = $user;
  $backup->session = session_encode();

  $_SESSION = array();

  $session_name = session_name();
  if(!isset($_COOKIE[$session_name])) {
    $_COOKIE[$session_name] = $sid;
  }

  session_id($sid);
  _drupal_session_read($sid);

  drupal_save_session(FALSE);

  if($user->uid == 0) {
    throw new UnauthenticatedException();
  }
}

class UnauthenticatedException extends Exception {}
class PermissionDeniedException extends Exception {}
