<?php

function entity_type_get_sharing_policy($type, $bundle = NULL) {
  $info = entity_get_info($type);

  $policy = !empty($info['entity keys']['bundle']) ?
    variable_get('remote_entities_entity_' . $type . '_bundle_' . $bundle):
    variable_get('remote_entities_entity_type_' . $type);

  return $policy === NULL ? 'share_none' : $policy;
}

/**
 * Checks if the entity is shared.
 *
 * @param string $entity_type
 * @param int $entity_id
 * @return bool
 */
function entity_is_shared($entity_type, $entity_id) {
  $entity = entity_load($entity_type, array($entity_id));
  $entity = array_shift($entity);
  $info = entity_get_info($entity_type);

  $policy = entity_type_get_sharing_policy($entity_type,
    !empty($info['entity keys']['bundle']) ? $entity->{$info['entity keys']['bundle']} : NULL);

  if($policy == 'share_all') {
    return TRUE;
  }
  if($policy == 'share_none') {
    return FALSE;
  }

  $query = db_select('remote_entity_bundle_policy', 'bp')
    ->condition('bp.type', $entity_type)
    ->condition('bp.id', $entity->{$info['entity keys']['id']});
  $query->addExpression('COUNT(*)');
  if(!empty($info['entity keys']['revision']) && !empty($entity->{$info['entity keys']['revision']})) {
    $query->condition('bp.revision', $entity->{$info['entity keys']['revision']});
  }
  if(isset($bundle)) {
    $query->condition('bp.bundle', $bundle);
  }

  return ($policy == 'share_all_except') ^ $query->execute()->fetchField();
}

interface RemoteEntityServer {
}

/**
 * Factory to get a server classname.
 *
 * @return string
 */
function remote_entities_get_server() {
  $cache = &drupal_static(__FUNCTION__, NULL);
  if($cache === NULL) {
    $servers = module_invoke_all('entity_server');
    $servers = is_array($servers) ?
      array_keys($servers):
      array();
    $proposed = variable_get('remote_entities_entity_server');
    if(in_array($proposed, $servers)) {
      $cache = $proposed;
    }
    else {
      $cache = remote_entities_services_available() ?
        'ServicesEntityServer':
        'FallbackEntityServer';
    }
  }
  return $cache;
}

abstract class AbstractEntityServer implements RemoteEntityServer {

}

/**
 * A fallback entity server.
 *
 * Note, that this server is only intended for testing
 * or debugging purposes. For instance it does not have
 * a proper authentication method.
 */
class FallbackEntityServer extends AbstractEntityServer {

}

class ServicesEntityServer extends AbstractEntityServer {

}