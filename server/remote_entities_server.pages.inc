<?php

/**
 * Page callback for admin/config/services/remote-entities-server
 *
 * @return array
 */
function remote_entities_server_settings_form() {
  $form = array();

  $form['remote_entities_entity_server'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('remote_entities_entity_server'),
    '#title' => t('Server type'),
    '#options' => drupal_map_assoc(array_keys(module_invoke_all('entity_server'))),
  );

  foreach(entity_get_info() as $type => $data) {
    $form['remote_entities_entity_' . $type] = array(
      '#title' => $data['label'],
      '#type' => 'checkbox',
      '#default_value' => variable_get('remote_entities_entity_' . $type),
    );
    $generator = function($label, $type, $variable_name, $share_none = FALSE) {
      $options = array(
        'share_all' => t('Share all'),
        'share_all_except' => t('Share all except selected items'),
        'share_none_except' => t('Share none except selected items'),
      );
      if($share_none) {
        $options['share_none'] = t('Disable sharing');
      }
      $default_value = variable_get($variable_name);
      return array(
        '#type' => 'radios',
        '#title' => $label,
        '#default_value' => $default_value?
                            $default_value:
                            ($share_none ? 'share_none' : NULL),
        '#options' => $options,
        '#states' => array(
          'visible' => array(
            ":input[name=remote_entities_entity_$type]" => array('checked' => TRUE),
          ),
        ),
      );
    };
    if(!empty($data['entity keys']['bundle'])) {
      foreach($data['bundles'] as $bundle_name => $bundle) {
        $form['remote_entities_entity_' . $type . '_bundle_' . $bundle_name] =
          $generator(
            $bundle['label'],
            $type,
            'remote_entities_entity_' . $type . '_bundle_' . $bundle_name,
            TRUE
          );
      }
    }
    else {
      $form['remote_entites_entity_type_' . $type] =
        $generator('', $type, 'remote_entites_entity_type_' . $type);
    }
  }

  return system_settings_form($form);
}

